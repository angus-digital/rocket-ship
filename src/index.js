import Rocket from 'components/Rocket';
import 'main.sass';

const main = async () => {
    document.title = 'Rocket Ship!';
    Rocket('#Turnstone');
}

main().then(() => console.log('Started'));