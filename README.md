# Rocket Ship

This is an animation of a rocket ship.\
Also comes with a remote launch button! 🥝

### Installation

Clone this repo

```bash
git clone git@gitlab.com:angus-digital/rocket-ship
```

### Getting started

Install dependencies

```bash
yarn install
```

Run local development server

```bash
yarn dev
```

Build for production

```bash
yarn build
```

### Contributing

Open to any contributions! To submit your work: fork this repository, commit your work, create a PR!
