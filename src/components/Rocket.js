async function Rocket(label) {
    document.body.innerHTML =  `<div id="rocket">
        <div class="rocket-body">
            <div class="body"></div>
            <div class="fin fin-left"></div>
            <div class="fin fin-right"></div>
            <div class="window"><img src="./turnstone.png" alt="Ruddy Turnstone" class="turnstone" /></div>
            <div class="label">${label}</div>
        </div>
        <div class="exhaust-flame"></div>
        <ul class="exhaust-fumes">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
    <button type="button" id="launch-button">Launch 🚀</button>`;

    var button = document.getElementById('launch-button');
    var rocket = document.getElementById('rocket');
    var stopLaunch = () => rocket.classList.remove('take-off');

    button.addEventListener('click', () => rocket.classList.add('take-off'));
    rocket.addEventListener('webkitAnimationEnd', stopLaunch);
    rocket.addEventListener('animationend', stopLaunch);
    rocket.addEventListener('click', () => rocket.classList.toggle('take-off'));
}

export default Rocket;